class OrderItem < ApplicationRecord
  belongs_to :product
  belongs_to :order

  # new products need to have a code, name, price and stock amount
  validates :product_id, :order_id, :amount, :sold_price, presence: true

  # amount has to be an integer
  validates :amount, numericality: { only_integer: true }
end
