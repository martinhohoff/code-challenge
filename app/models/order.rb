class Order < ApplicationRecord
  has_many :order_items, dependent: :destroy
  has_many :products, through: :order_items

  # new orders need to have a code, buyer name, status, shipping cost
  validates :code, :buyer, :status, :shipping_cost, presence: true

  # each order needs to have a unique code, an integer number
  validates :code, uniqueness: true, numericality: { only_integer: true }

  # shipping cost has to be a number, can be a float
  validates_numericality_of :shipping_cost

  # receives an order object and a hash with product_id-amount and price hashes
  # needs to delete products that were in the old order
  # but are not in the updated order
  def update_items(ids_prices_and_amounts_array)
    delete_old_order_items(ids_prices_and_amounts_array)
    update_order_items(ids_prices_and_amounts_array)
  end

  # receives an order object and a hash with product_id-amount and price hashes
  # creates new order items for each
  def create_items(ids_prices_and_amounts_array)
    create_new_order_items(ids_prices_and_amounts_array)
  end

  private

  def delete_old_order_items(ids_prices_and_amounts_array)
    new_products_ids = []
    ids_prices_and_amounts_array.each do |product_id_price_and_amount|
      product_id = product_id_price_and_amount[:product_id]
      new_products_ids << product_id
    end
    self.order_items.each do |old_order|
      unless new_products_ids.include?(old_order.product_id)
        old_order.destroy
      end
    end 
  end

  def create_new_order_items(ids_prices_and_amounts_array)
    ids_prices_and_amounts_array.each do |product_id_price_and_amount|
      id = product_id_price_and_amount[:product_id]
      amount = (product_id_price_and_amount[:amount]).to_i
      sold_price = product_id_price_and_amount[:sold_price]

      # create the order item
      self.order_items.create!(product_id: id, amount: amount, sold_price: sold_price)

      product = Product.find(id)
      
      # in both cases, we decrease the stock amount at the end
      product.stock -= amount
      product.save
    end
  end

  def update_order_items(ids_prices_and_amounts_array)
    ids_prices_and_amounts_array.each do |product_id_price_and_amount|
      id = product_id_price_and_amount[:product_id]
      amount = (product_id_price_and_amount[:amount]).to_i
      sold_price = product_id_price_and_amount[:sold_price]
      
      # finds the corresponding product
      product = Product.find(id)
      
      # if products is already on order, we update it
      if self.products.include?(product)
        current_order_item = self.order_items.where(product_id: product.id).first

        # first we "return" the amount to the stock, and destroys the old 
        product.stock += current_order_item.amount
        product.save
        
        # then change the attributes
        current_order_item.amount = amount
        current_order_item.sold_price = sold_price
        current_order_item.save
      else
        # if products is not on order, we just create the order item
        self.order_items.create!(product_id: id, amount: amount, sold_price: sold_price)
      end

      # in both cases, we decrease the stock amount at the end
      product.stock -= amount
      product.save      
    end
  end
end
