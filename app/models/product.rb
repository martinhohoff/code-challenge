class Product < ApplicationRecord
	has_many :order_items, dependent: :destroy
	has_many :orders, through: :order_items

	# new products need to have a code, name, price and stock amount
	validates :code, :name, :price, :stock, presence: true
	
	# each product needs to have a unique code, an integer number
	validates :code, uniqueness: true

	# code and stock need to be integers
	validates :code, :stock, numericality: { only_integer: true }

	# price has to be a number, can be a float	
	validates_numericality_of :price
end
