class Api::V1::ReportController < Api::V1::BaseController
  def show
  	@start_date = params[:start_date].to_date
  	@end_date = params[:end_date].to_date

  	report_orders = Order.where("purchase_date > ?", @start_date).where("purchase_date < ?", @end_date)

    @number_of_orders = report_orders.count

    # "Ticket médio é a métrica que apresenta o valor médio que cada cliente gasta em suas compras no seu estabelecimento."
    # https://clubesebrae.com.br/blog/ticket-medio-voce-planeja-suas-vendas-com-base-nesta-metrica
    if @number_of_orders == 0
      @total_revenue = 0
      @average_ticket = 0 # prevent division by 0
    else
      @total_revenue = get_total_revenue(report_orders)

      # to_f prevents the possibility of integer division if @total is an integer
      # round(2) gives two decimal points
      @average_ticket = (@total_revenue / (@number_of_orders).to_f).round(2)
    end
  end

  private

  def get_total_revenue(report_orders)
    total_revenue = 0

    report_orders.each do |order|
      # first, add the price for shipping
      total_revenue += order.shipping_cost
      
      # then, add the sub-total for every item in all orders
      order.order_items.each do |item|
        sub_total = item.sold_price * item.amount
        total_revenue += sub_total
      end
    end

    return total_revenue
  end

  def report_params
    params.require(:report).permit(:start_date, :end_date)
  end
end