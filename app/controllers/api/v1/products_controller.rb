class Api::V1::ProductsController < Api::V1::BaseController
  before_action :set_product, only: [ :show, :update, :destroy ]

  def index
    @products = Product.all
  end

  def show
  end

  def update
    if @product.update(product_params)
      render :show
    else
      render_error
    end
  end

  def create
    @product = Product.new(product_params)
  	
    if @product.save
      render :show, status: :created
    else
      render_error
    end
  end

  def destroy    
    if @product.destroy
      # if successfully deleted, will render empty response
      head :no_content
    else
      render_error
    end
  end

  private

  def set_product
  	@product = Product.find(params[:id])
  end

  def render_error
    render json: { errors: @product.errors.full_messages },
      status: :unprocessable_entity
  end

  def product_params
    params.require(:product).permit(:code, :name, :description, :stock, :price, :product_attributes)
  end
end