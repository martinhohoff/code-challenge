class Api::V1::OrdersController < Api::V1::BaseController
  before_action :set_order, only: [ :show, :update, :destroy ]

  def index
    @orders = Order.all
  end

  def show
  end

  def update
    if has_stock?
      if @order.update(params_without_items)
        # calls a method to build placements inside the new order object
        @order.update_items(params_items)
        
        render :show
      else
        render_error
      end
    else
      render json: { errors: "Product does not exist, does not have sufficient stock, or amount is invalid." }
    end
  end

  def create
    # instantiates a new Order object
    @order = Order.new(params_without_items)
    # checking for model validation conditions and stock existence
    if has_stock?
      if @order.save
        # calls a method to build placements inside the new order object
        @order.create_items(params_items)
        render :show, status: :created
      else
        render_error
      end
    else
      render json: { errors: "Product does not exist, does not have sufficient stock, or amount is invalid." }
    end
  end

  def destroy    
    if @order.destroy
      # if successfully deleted, will render empty response
      head :no_content
    else
      render_error
    end
  end

  private

  def set_order
  	@order = Order.find(params[:id])
  end

  def has_stock?
    # order needs to include products
    if params_items.nil? || params_items.empty?
      return false
    end

    # separates id and amount for each pair, related to the order
    params_items.each do |product_id_price_and_amount|
      id = product_id_price_and_amount[:product_id]
      
      if Product.where(id: id).any?
        #check for the existence of product with that id
        # finds the corresponding product
        product = Product.find(id)
        
        amount = (product_id_price_and_amount[:amount]).to_i

        stock = product.stock

        # if the product is already in the order,
        # we "add" the amount already ordered to stock 
        # before calculating 
        if @order.products.include?(product)
          amount_already_ordered = @order.order_items.where(product_id: product.id).first.amount
          stock += amount_already_ordered
        end

        if stock < amount || amount <= 0 # product not in stock or invalid order amount
          return false
        end
      else
        return false
      end
    end
    return true
  end

  def params_without_items
    # this method will return all query parameters, except "order_items"
    code = order_params[:code]
    buyer = order_params[:buyer]
    status = order_params[:status]
    shipping_cost = order_params[:shipping_cost]
    
    return {code: code, buyer: buyer, status: status, shipping_cost: shipping_cost}
  end

  def params_items
    # this method will consider only "order_items" as params
    params[:order][:order_items]
  end

  def render_error
    render json: { errors: @order.errors.full_messages },
      status: :unprocessable_entity
  end

  def order_params
    params.require(:order).permit(:code, :buyer, :status, :shipping_cost, { order_items: [:product_id, :amount, :sold_price] })
  end
end