json.array! @products do |product|
  json.extract! product, :id, :code, :name, :description, :stock, :price, :product_attributes
end
