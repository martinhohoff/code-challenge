json.report do
	json.start_date @start_date
	json.end_date @end_date
	json.total_revenue @total_revenue
	json.number_of_orders @number_of_orders
	json.average_ticket @average_ticket
end
