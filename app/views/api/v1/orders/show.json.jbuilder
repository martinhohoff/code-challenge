json.extract! @order, :id, :code, :buyer, :status, :shipping_cost
  json.order_items @order.order_items do |item|
  	json.extract! item, :product_id, :amount, :sold_price
  end