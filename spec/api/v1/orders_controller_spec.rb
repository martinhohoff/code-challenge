require 'rails_helper'

require 'json'

class OrdersControllerTest < ActionController::TestCase

	order_params = 	{ "order": 
		{
		    "code": 1233234,
		    "buyer": "Anita Knapp",
		    "status": "cancelado'",
		    "shipping_cost": "18.15",
		    "order_items": [
		        {
		            "product_id": 4,
		            "amount": 1,
		            "sold_price": 2
		        }
		    ]
		}
	}

  	describe "CRUD" do
	  	describe "Create order" do

	  		it "Gets a 201 http response" do
		  		post api_v1_orders_path, params: order_params
		  		expect(response).to have_http_status(201)
		  	end

			it "Creates a new order in the database" do
				# creates a new code to test the params
				order_params[:order][:code] = 555554

				number_of_orders = Order.count
				post api_v1_orders_path, params: order_params
				
				# finds all items in database
				expect(Order.count).to eq (number_of_orders += 1)
			end

			it "Decreases stock for an ordered product" do
				# changes params to include a new code
				order_params[:order][:code] = 674574567
				
				product_id = order_params[:order][:order_items].first[:product_id].to_i
				ordered_product = Product.find(product_id)
				initial_stock = ordered_product.stock
				amount_ordered = order_params[:order][:order_items].first[:amount]	

				post api_v1_orders_path, params: order_params
				
				new_stock = Product.find(product_id).stock 

				# check that stock has decreased
				expect(new_stock).to eq (initial_stock - amount_ordered)
			end

			it "Does not create order if product has not been registered" do
				# changes params to include a new code
				order_params[:order][:code] = 99999
				# changes params to an invalid product id
				order_params[:order][:order_items].first[:product_id] = 9999

				post api_v1_orders_path, params: order_params
				
				expect(JSON.parse(response.body)["errors"]).to eq "Product does not exist, does not have sufficient stock, or amount is invalid."
			end

			it "Does not create order if product does not have enough stock" do
				# changes params to include a new code
				order_params[:order][:code] = 9856356
				
				# changes params to use the last product created
				order_params[:order][:order_items].first[:product_id] = Product.last.id

				# changes amount to a value greater than the stock of the last product
				order_params[:order][:order_items].first[:amount] = Product.last.stock += 1

				

				post api_v1_orders_path, params: order_params
				
				expect(JSON.parse(response.body)["errors"]).to eq "Product does not exist, does not have sufficient stock, or amount is invalid."
			end
		end

		describe "Show order" do

		  	it "Gets a 200 http response" do
		  		order = Order.last
		  		get api_v1_order_path(order)
		  		expect(response).to have_http_status(200)
		  	end

		  	it "Gets the last created order" do 
		  		order = Order.last

		  		get api_v1_order_path(order)
		  		json_response = JSON.parse(response.body)

		  		json_response[:id] == order.id
		 	end
		end

		describe "Orders Index" do

		  	it "Gets a 200 http response" do 
		  		get api_v1_orders_path
		  		expect(response).to have_http_status(200)
		  	end

		  	it "Gets an array as response" do 
		  		get api_v1_orders_path
		  		json_response = JSON.parse(response.body)

		  		expect(json_response.class).to eq Array
		 	end

		 	it "Array contains all objects" do 
		  		get api_v1_orders_path
		  		json_response = JSON.parse(response.body)

		  		expect(json_response.count).to eq (Order.count)
		 	end
		end

		describe "Update order" do

			new_params = { "order": 
				{
				    "code": 777777,
				    "buyer": "John Doe",
				    "status": "cancelado'",
				    "shipping_cost": "18.15",
				    "order_items": [
				        {
				            "product_id": 4,
				            "amount": 1,
				            "sold_price": 30
				        }
				    ]
				}
			}

		  	it "Gets a 200 http response" do 
		  		order = Order.last
		  		patch api_v1_order_path(order), params: new_params

		  		expect(response).to have_http_status(200)
		  	end

		  	it "Updates the last order" do 
		  		order = Order.last
		  		patch api_v1_order_path(order), params: new_params

		  		json_response = JSON.parse(response.body)

		  		json_response["buyer"] == new_params[:order][:buyer]
		 	end
		end

		describe "Delete order" do

		  	it "Gets a 204 (no content) http response" do 
		  		order = Order.last
		  		delete api_v1_order_path(order) 
		  		expect(response).to have_http_status(204)
		  	end

		  	it "Deletes the last order from the DB" do 
		  		initial_amount_of_orders = Order.count
		  		order = Order.last

		  		delete api_v1_order_path(order) 
		  		expect(Order.count).to eq (initial_amount_of_orders - 1)
		 	end
		end
  	end
end