require 'rails_helper'

require 'json'

class ReportControllerTest < ActionController::TestCase

	report_params = {
		"start_date": "2018-01-01",
		"end_date": "2018-06-01"
	}

	describe "Report" do
	  	describe "Read the report" do
	  		it "Gets a 201 http response" do
		  		post api_v1_report_path, params: report_params
		  		expect(response).to have_http_status(200)
		  	end

		  	it "Includes average ticket" do
		  		post api_v1_report_path, params: report_params
		  		json_response = (JSON.parse(response.body))["report"]
		  		expect(json_response).to include ("average_ticket")
		  	end

		  	it "Calculates the correct average ticket" do
		  		start_date = report_params[:start_date]
		  		end_date = report_params[:end_date]

		  		orders_during_period = Order.where("purchase_date > ?", start_date).where("purchase_date < ?", end_date)
		  		number_of_orders = orders_during_period.count

		  		if number_of_orders == 0
		  			real_average_ticket = 0
		  		else
			  		# starts calculating the total revenue
			  		total_revenue = 0

				    orders_during_period.each do |order|
				      # first, add the price for shipping
				      total_revenue += order.shipping_cost
				      
				      # then, add the sub-total for every item in all orders
				      order.order_items.each do |item|
				        sub_total = item.sold_price * item.amount
				        total_revenue += sub_total
				      end
				    end
				    # end of total revenue calculation

				    # to_f prevents the possibility of integer division if total_revenue is an integer
				    # round(2) gives two decimal points
				    real_average_ticket = (total_revenue / number_of_orders.to_f).round(2)
				end

		  		post api_v1_report_path, params: report_params
		  		reported_average_ticket = ((JSON.parse(response.body))["report"]["average_ticket"]).to_f
		  		expect(reported_average_ticket).to eq (real_average_ticket)
		  	end
	  	end
	end
end