require 'rails_helper'

require 'json'

class ProductsControllerTest < ActionController::TestCase

	apple_params = { "product": 
		{
		"code": 12223,
		"name": "Maçã", 
		"price": "1.5", 
		"stock": "10",
		"description": "Maçã argentina.",
		"product_attributes": "vermelha, redonda, deliciosa"
		} 
	}

  	describe "CRUD" do
	  	describe "Create product" do
	  		it "Gets a 201 http response" do
		  		post api_v1_products_path, params: apple_params
		  		expect(response).to have_http_status(201)
		 	end

			it "Creates a new product in the database" do
				# create a new code to test
				apple_params[:product][:code] = 7777

				amount_of_products = Product.count
				post api_v1_products_path, params: apple_params
				expect(Product.count).to eq (amount_of_products + 1)
			end
		end

		describe "Show product" do
		  	it "Gets a 200 http response" do
		  		product = Product.last

		  		get api_v1_product_path(product)
		  		
		  		expect(response).to have_http_status(200)
		 	end

		  	it "Shows the last product" do 
		  		product = Product.last

		  		get api_v1_product_path(product)
		  		json_response = JSON.parse(response.body)

		  		json_response.each_key do |key|
		  			expect(json_response[key]).to eq product[key]
		  		end
		 	end
		end

		describe "Products Index" do
		  	it "Gets a 200 http response" do
		  		get api_v1_products_path
		  		
		  		expect(response).to have_http_status(200)
		 	end

		  	it "Gets an array as response" do 
		  		get api_v1_products_path
		  		json_response = JSON.parse(response.body)

		  		expect(json_response.class).to eq Array
		 	end

		 	it "Array contains all objects" do 
		  		get api_v1_products_path
		  		json_response = JSON.parse(response.body)
		  		expect(json_response.count).to eq (Product.count)
		 	end
		end

		describe "Update product" do
			new_params = { 
				"product": 
					{
						code: 98043,
						name: 'Maçã argentina', 
						price: "1", 
						stock: "9",
						description: "Maçã argentina perto do prazo de validade.",
						product_attributes: "suspeita, questionável"
					} 
			}

			it "Gets a 200 http response" do
		  		last = Product.last
		  		patch api_v1_product_path(last), params: new_params
		  		
		  		expect(response).to have_http_status(200)
		 	end

		  	it "Returns the updated object" do 
		  		last = Product.last
		  		patch api_v1_product_path(last), params: new_params

		  		json_response = JSON.parse(response.body)

		  		expect(json_response["name"]).to eq 'Maçã argentina'
		 	end
		end

		describe "Delete product" do
		  	it "Gets a 204 (no content) http response" do
		  		product = Product.last
		  		delete api_v1_product_path(product) 
		  		expect(response).to have_http_status(204)
		 	end

		  	it "Deletes the last product form the DB" do 
		  		product = Product.last
		  		delete api_v1_product_path(product) 
		  		expect(Product.last == product).to eq false
		 	end
		end
  	end
end