require 'rails_helper'  
require 'rspec-rails'
require 'faker'

DatabaseCleaner.strategy = :truncation

# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
RSpec.configure do |config|
  config.include ApiHelper, type: :api
  config.include Requests::JsonHelpers, type: :api

  config.before(:suite) do
  	# starts tests with a clean database and adds seeds to it
    DatabaseCleaner.clean
    load "#{Rails.root}/db/seeds.rb"
  end
  
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
end
