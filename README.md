Skyhub Code Challenge - Martinho Hoffman
==================

- - - 

## Loja do Seu Manuel - API v1

#### API interna para gestão de produtos e pedidos em Ruby on Rails, primeira versão.

- - - 

## INSTALAÇÃO

Clone o repositório em seu computador, instale as gemas do projeto, crie a database e inicie o servidor Rails.

	git clone https://bitbucket.org/martinhohoff/code-challenge.git

	cd code-challenge

	bundle

	rails db:create db:migrate

	rails s

TESTES

	rspec

caso isso gere uma mensagem de migrações pendentes, execute as migrações no ambiente de teste antes de abrir o rspec

	rails db:migrate RAILS_ENV=test

	rspec

Ao serem carregados, os testes do *rspec* vão sempre gerar um banco de dados vazio e povoá-lo com dados de exemplo (seeds), exatamente 10 produtos e 10 pedidos. 

As seeds também podem ser produzidas usando:

	rails db:seed

A configuração inicial produz 10 *novos* produtos de teste e 10 *novos* pedidos a cada execução.

As propriedades e quantidades dos exemplos podem ser alteradas em db/seeds.rb.

- - - 

## ENDPOINTS

Utilize o browser ou um serviço como o Postman para acessar os endpoints abaixo:

#### Produtos

GET /api/v1/products - Retorna todos os produtos cadastrados		

GET /api/v1/products/:id - Exibe informações sobre um produto 

PATCH /api/v1/products/:id - atualiza informações de um produto existente

POST /api/v1/products - cria um novo produto

DELETE /api/v1/products/:id - deleta um produto existente (retorna no-content)


#### Pedidos

GET /api/v1/orders - Retorna todos os pedidos cadastrados		

GET /api/v1/orders/:id - Exibe informações sobre um pedido 

PATCH /api/v1/orders/:id - atualiza informações de um pedido existente

POST /api/v1/orders - cria um novo pedido

DELETE /api/v1/orders/:id - deleta um produto existente (retorna no-content)


#### Relatórios

POST /api/v1/report - abre um relatório de ticket médio 

Ex.: 

	POST http://localhost:3000/api/v1/report/

	{"start_date": "2018-01-01",
	"end_date": "2018-06-01"}

- - - 


*ATENÇÃO: id != code*

ID é a identificação do objeto no banco de dados.

CODE é o código customizável do produto na loja (número inteiro).

- - - 

## FORMATOS

Todas as entradas e saídas são realizadas utilizando JSON.

### Produtos 

Exemplo de entrada:

	POST http://localhost:3000/api/v1/products/

	{ "product": 
		{
        "code": 1601,
        "name": "Coxinha",
        "description": "Massa frita recheada com frango desfiado e catupiry.",
        "stock": 15,
        "price": "4.50",
        "product_attributes": "frita, calórica, salgada, barata"
    	}
	}

Exemplo de saída:

	GET http://localhost:3000/api/v1/products/12

	{
	    "code": 1601,
	    "name": "Coxinha",
	    "description": "Massa frita recheada com frango desfiado e catupiry.",
	    "stock": 15,
	    "price": "4.5",
	    "product_attributes": "frita, calórica, salgada, barata"
	}

	
### Pedidos 

Exemplo de entrada:

	PATCH http://localhost:3000/api/v1/orders/3

	{ "order": 
		{
		    "code": 1234,
		    "buyer": "Anita Knapp",
		    "status": "cancelado'",
		    "shipping_cost": "18.15",
		    "order_items": [
		        {
		            "product_id": 1,
		            "amount": 1,
		            "sold_price": 2
		        }
		    ]
		}
	}


Exemplo de saída:

	GET http://localhost:3000/api/v1/orders/1

	{
	    "id": 14,
	    "code": 1234,
	    "buyer": "Anita Knapp",
	    "status": "cancelado'",
	    "shipping_cost": "18.15",
	    "order_items": [
	        {
	            "product_id": 1,
	            "amount": 1,
	            "sold_price": "2.0"
	        }
	    ]
	}


### Relatórios

exemplo de entrada:

	POST http://localhost:3000/api/v1/report/

	{"start_date": "2018-01-01",
	"end_date": "2018-06-01"}

exemplo de saída:

	{
	    "report": {
	        "start_date": "2018-01-01",
	        "end_date": "2018-06-01",
	        "total_revenue": "2387.2",
	        "number_of_orders": 4,
	        "average_ticket": "596.8"
	    }
	}

- - - 

## MODELOS DO BANCO DE DADOS

#### Produtos (products):

* possuem código ("code"), nome do produto ("name"), descrição do produto ("description"), quantidade em estoque ("stock"), preço ("price") e atributos ("product_attributes")

* código, nome, preço e quantidade no estoque são obrigatórios para cadastrar um item

* código precisa ser único

* código e estoque precisam ser números inteiros

* preço precisa ser um número 



#### Pedidos (orders):

* possuem código ("code"), data de compra ("purchase_date"), nome do comprador ("buyer"), status ("status"), custo de frete ("shipping_cost")

* possuem itens (products) por meio da tabela intermediáris OrderItems

* cada item possui uma quantidade (amount)

* cada item tem um preço de venda. É o preço pelo qual o produto foi vendido - não necessariamente o preço de catálogo.



#### Itens de pedido (order_items):

* apontam como referência um pedido e um produto

* funciona como tabela intermediária entre produto e pedido

* possuem id do produto(product_id), id do pedido (order_id), quantidade (amount) e preço de venda (price_sold)

* todos os campos são obrigatórios


- - - 
	

