Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
  	namespace :v1 do
  	  resources :products, only: [ :index, :show, :update, :create, :destroy ]
  	  resources :orders, only: [ :index, :show, :update, :create, :destroy ]
  	  post "/report", to: "report#show"
  	end
  end
end
