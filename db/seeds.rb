# create n product seeds
def create_sample_products(n)
  puts "Creating #{n} products..."
  10.times do |i|
    while true
      # this loop will assure a unique code for the new product created
    	new_code = rand(0..9999)
    	break if Product.where(code: new_code).empty?
    end
  
    # effectively creating the product
    product = Product.create!(
      code: new_code,
      name: Faker::Food.dish,
      description: Faker::Food.description,
      stock: 20,
      price: rand(1.0..100.0).round(2), # creates a float price
      product_attributes: Faker::Lorem.words(4).join(', ')
    )
    puts "#{i + 1}. #{product.name}"
  end
  puts 'Finished!'
end


# create n order seeds with up to x products
def create_sample_orders(n, x)
  puts "Creating #{n} orders..."
 
  n.times do |i|
    # the loop below will assure a unique code for the new order created
    while true
      new_code = rand(0..9999)
      break if Order.where(code: new_code).empty?
    end

    # effectively creating the order
    order = Order.create!(
      code: new_code,
      purchase_date: Faker::Date.between(1.years.ago, Date.today),
      buyer: Faker::FunnyName.name,
      status: %w('novo aprovado entregue cancelado').sample,
      shipping_cost: rand(10.0..50.0).round(2), # creates a float shipping cost
    )

    # start with an empty array of products
    product_ids_prices_and_amounts = []
  
    # adds 1 to 5 products to that array
    rand(1..x).times do
      while true
        ordered_product = Product.all.sample
        amount = 1

        # assure we don't add a product without stock        
        break unless ordered_product.stock == 0
      end

      sold_price = rand(0.8..1) * ordered_product.price # allows for a random discount up to 20% on product price
    
      product_ids_prices_and_amounts << {product_id: ordered_product.id, amount: amount, sold_price: sold_price}
    end

    order.create_items(product_ids_prices_and_amounts)
    order.save

    puts "#{i + 1}. #{order.products.count} items"


  end
  puts 'Finished!'
end

# create 10 products
create_sample_products(10)

# create 10 orders with up to 5 products
create_sample_orders(10, 5)