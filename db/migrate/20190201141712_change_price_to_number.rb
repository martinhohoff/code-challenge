class ChangePriceToNumber < ActiveRecord::Migration[5.2]
  def change
    change_column :products, :price, :float
    change_column :orders, :shipping_cost, :float
  end
end
