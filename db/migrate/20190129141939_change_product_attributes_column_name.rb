class ChangeProductAttributesColumnName < ActiveRecord::Migration[5.2]
  def change
  	rename_column :products, :attributes, :product_attributes
  end
end
