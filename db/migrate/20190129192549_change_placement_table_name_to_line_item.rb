class ChangePlacementTableNameToLineItem < ActiveRecord::Migration[5.2]
  def change
  	rename_table :placements, :line_items
  end
end
