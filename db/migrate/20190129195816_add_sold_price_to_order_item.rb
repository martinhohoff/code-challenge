class AddSoldPriceToOrderItem < ActiveRecord::Migration[5.2]
  def change
  	add_column :order_items, :sold_price, :decimal, precision: 10, scale: 2
  end
end
