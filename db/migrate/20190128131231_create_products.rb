class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.integer :code
      t.string :name
      t.string :description
      t.integer :stock
      t.decimal :price, precision: 10, scale: 2
      t.string :attributes

      t.timestamps
    end
  end
end
