class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :code
      t.datetime :purchase_date
      t.string :buyer
      t.string :status
      t.decimal :shipping_cost, precision: 10, scale: 2

      t.timestamps
    end
  end
end
